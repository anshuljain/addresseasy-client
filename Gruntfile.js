module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
            bundle: {
                src: ['bower_components/typeahead.js/dist/typeahead.bundle.min.js', 
                      'src/addresseasy-client.js'
                ],
                dest: 'dist/addresseasy-client.js'
            }
        },
        uglify: {
            dist: {
                src: 'dist/*.js',
                dest: 'dist',
                expand: true,
                flatten: true,
                ext: '.min.js'
            }
        },
        cssmin: {
            target: {
                files: {
                    'dist/addresseasy-client.min.css': ['src/addresseasy-client.css']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');


    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);

};
