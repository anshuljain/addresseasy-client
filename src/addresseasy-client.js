var addresseasy = function() {
    var $el;

    var api = {};

    api.data = [];

    api.init = function (selector, options) {
        $el = jQuery(selector);
        var addrs = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: options.url + '?q=%QUERY',
                wildcard: '%QUERY',
                rateLimitBy: 100,
                transform: function (response) {
                    api.data = response;
                    return response.map(function (a) {
                        return a.formatted;
                    });
                }
            }
        });

        $el.typeahead(
            {
                minLength: 5,
                highlight: true,
                hint: true
            },
            {
                name: 'addresses',
                source: addrs,
                templates: {
                    footer: [
                      '<div class="powered-by">',
                        'Powered by <a href="https://addresseasy.com.au">AddressEasy</a>',
                      '</div>'
                    ].join('\n'),

                    notFound: [
                      '<div class="empty-message">',
                        'Sorry, no addresses matched your search. Please try again.',
                      '</div>'
                    ].join('\n'),
                }
            }
        )
        .on('typeahead:asyncrequest', function() {
            $el.css('background', 'url("' + options.spinner + '") no-repeat right center');
        })
        .on('typeahead:asynccancel typeahead:asyncreceive', function() {
            $el.css('background', '');
        });
    };

    api.onSelect = function(cb) {
        $el.bind('typeahead:select', function(ev, suggestion) {
            for (var i = 0; i < api.data.length; i++) {
                if (api.data[i].formatted == suggestion) {
                    cb(api.data[i]);
                    return;
                }
            }
        });
    };

    return api;
}();

var cssId = 'addresseasy-css'; 
if (!document.getElementById(cssId)) {
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.id   = cssId;
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://cdn.addresseasy.com.au/css/addresseasy-client.min.css';
    link.media = 'all';
    head.appendChild(link);
} else {
    console.log(cssId + ' element id is already being used');
}
